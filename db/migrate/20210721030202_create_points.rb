class CreatePoints < ActiveRecord::Migration[5.1]
  def change
    create_table :points do |t|
      t.integer :point
      t.references :user, foreign_key: true

      t.timestamps
    end
    add_index :points, [:user_id, :created_at]
  end
end
