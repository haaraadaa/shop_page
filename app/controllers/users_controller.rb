# coding: utf-8
class UsersController < ApplicationController
  before_action :logged_in_user, only: [:edit, :update, :destroy]
  before_action :correct_user,   only: [:edit, :update]
  
  def show
    @user = User.find(params[:id])
  end
  
  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      @user.point.create!(point: 10)
      log_in @user
      flash[:success] = "登録ありがとうございます。"
      redirect_to @user
    else
      render 'new'
    end
  end
  
  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "プロフィール編集完了いたしました。"
      redirect_to @user
    else
      render 'edit'
    end
  end

  private
   def user_params
     params.require(:user).permit(:name, :email, :password, :password_confirmation, :image)
   end

   #beforeアクション

   #ログイン済みユーザーかどうか
   def logged_in_user
     unless logged_in?
       store_location
       flash[:danger] = ""
       redirect_to login_url
     end
   end

   def correct_user
     @user = User.find(params[:id])
     redirect_to(root_url) unless current_user == @user
   end

end
