# coding: utf-8
class StaticPagesController < ApplicationController
  before_action :logged_in_user, only: [:gacha]
  
  def home
  end

  def about
  end

  def access
  end

  def gacha
  end

  def lottery
    @user = current_user
    
    if user_params[:point].to_s =~ /^[0-9]+$/
      @user.point.create!(point: user_params[:point].to_i)
      flash[:notice] = "#{user_params[:point].to_i}ポイントを取得しました。"
      redirect_to gacha_url
    else
      flash[:danger] = "整数でないです" #テスト
      redirect_to gacha_url
    end
  end

  private
   def user_params
     params.permit(:point)
   end

   #beforeアクション

   #ログイン済みユーザーかどうか
   def logged_in_user
     unless logged_in?
       store_location
       flash[:danger] = ""
       redirect_to login_url
     end
   end

end
