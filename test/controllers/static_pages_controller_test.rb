require 'test_helper'

class StaticPagesControllerTest < ActionDispatch::IntegrationTest

  test "should get root" do
    get root_path
    assert_response :success
    assert_select "title", "HOME | shop"
  end
  
  test "should get home" do
    get root_url
    assert_response :success
    assert_select "title", "HOME | shop"
  end

  test "should get about" do
    get about_path
    assert_response :success
    assert_select "title", "ABOUT | shop"
  end

  test "should get access" do
    get access_path
    assert_response :success
    assert_select "title", "ACCESS | shop"
  end
  
  test "should get signup" do
    get signup_path
    assert_response :success
    assert_select "title", "Sign up | shop"
  end

end
