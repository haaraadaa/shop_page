require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:michael)
  end
  
  test "home layout links" do
    get root_path
    assert_template 'static_pages/home'
    assert_select "a[href=?]", root_path, count: 1
    assert_select "a[href=?]", about_path
    assert_select "a[href=?]", access_path
    assert_select "a[href=?]", signup_path
  end

  test "index layout links" do
    log_in_as(@user)
    get root_path
    assert_select "a[href=?]", root_path, count: 1
    assert_select "a[href=?]", logout_path
  end
end
