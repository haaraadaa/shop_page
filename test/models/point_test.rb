require 'test_helper'

class PointTest < ActiveSupport::TestCase

  def setup
    @user = users(:michael)
    @point = Point.new(point: 10, user_id: @user.id)
  end

  test "point should be valid"  do
    assert @point.valid?
  end

  test "point should be present" do
    @point.user_id = "   "
    assert_not @point.valid?
  end

  test "point should be integer" do
    @point.point = 50
    assert @point.valid?
  end

  test "point should not present without integer" do
    @point.point = "aaaa"
    assert_not @point.valid?
  end

  
end
